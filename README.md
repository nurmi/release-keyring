# Release Keys

## Generating the key

Use the following command to generate your key file:
```
gpg --armor --export KEY_NAME > keys/USERNAME@key1.asc
```

To find out which KEY_NAME to use, you can run:

```
gpg --list-keys
```

## Changing existing keys and adding new ones

This keyring is intended to include all keys that have been used at some point or another for releasing KDE software (not just currently in use keys).

If you create a new key, you would therefore leave the old one as e.g. `USERNAME@key1.asc` and add the new one as, in this case, `USERNAME@key2.asc`.

If you wish you can also revoke your old key, in which case the revoked key would replace the original valid key. Same applies for extending the validity of an existing key.
